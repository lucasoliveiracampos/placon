import Vue from 'vue'

var logged_user = null;

function mockasync (data) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve({data: data}), 600)
  })
}

const all_data = {
    wishlist: []
}

const api = {
    login(username, password){
        if(password){
            logged_user = {
                username: username,
                first_name: 'Mark',
                last_name: 'Zuckerberg',
                email: 'zuck@facebook.com',
                notifications_enabled: true,
                permissions:{
                    ADMIN: username == 'admin',
                    STAFF: username == 'admin',
                }
            };
        }
        return mockasync(logged_user);
    },
    logout(){
        logged_user = null;
        return mockasync({});
    },
    whoami(){
        return mockasync(logged_user ? {
            authenticated: true,
            user: logged_user,
        } : {authenticated: false});
    },
    save_wishlist(wishlist){
        let new_wishlist = Object.assign({}, wishlist);        
        new_wishlist.save_money = new_wishlist.value / new_wishlist.days;
        if (new_wishlist.id){
            let finded_index = all_data.wishlist.findIndex(item => item.id === new_wishlist.id);
            if (finded_index > -1) {
                let actualWishlist = all_data.wishlist[finded_index];
                actualWishlist = new_wishlist;
                all_data.wishlist[finded_index] = actualWishlist;
            } else {
                console.log('não foi encontrado o id: ' + new_wishlist.id);
                return;
            }
        } else {
            new_wishlist.id = all_data.wishlist.length + 1; 
            all_data.wishlist.push(new_wishlist);
        }
        return mockasync({
            id: new_wishlist.id,
            name: new_wishlist.name,
            value: new_wishlist.value,
            days: new_wishlist.days,
            category: new_wishlist.category,
            save_money: new_wishlist.save_money
        });
    },
    list_wishlist(){
        return mockasync({
            wishlist: JSON.parse(JSON.stringify(all_data.wishlist))
        });
    },
    edit_wishlist(wishlist){
        if (wishlist){
            return mockasync({wishlist});
        }
    },
    remove_wishlist(wishlist){
        if (wishlist){
            let finded_index = all_data.wishlist.findIndex(item => item.id === wishlist.id);
            if (finded_index > -1) { 
                all_data.wishlist.splice(finded_index, 1).length > 0;
            }
        }
        return mockasync(false);
    }
};

export default api;
