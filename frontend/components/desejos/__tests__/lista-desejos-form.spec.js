import Vue from 'vue'
import ListaDesejos from 'components/desejos/lista-desejos-lista.vue'

describe('MyComponent', () => {
    it('has a created hook', () => {
      expect(typeof ListaDesejos.created).toBe('function')
    })
  
    it('sets the correct default data', () => {
      expect(typeof ListaDesejos.data).toBe('function')
      const defaultData = MyComponent.data()
      const expected = {
        headers: [
          {text: 'Nome', value: 'name'},
          { text: 'Valor', value: 'value' },
          { text: 'Daqui quantos dias', value: 'days' },
          { text: 'Categoria', value: 'category' },
          { text: 'Economizar por dia', value: 'save_money' },
          { text: 'Actions', value: 'name', sortable: false }
        ],
        wishlist: []
      };
      expect(defaultData.message).toBe(expected)
    })
})
